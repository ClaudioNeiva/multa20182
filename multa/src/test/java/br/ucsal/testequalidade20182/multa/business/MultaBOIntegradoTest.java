package br.ucsal.testequalidade20182.multa.business;

public class MultaBOIntegradoTest {

	/*
	 * Método a ser testado: public void inserir(Integer numeroCnh, Date
	 * dataHora, String local, TipoMultaEnum tipoMulta) throws
	 * RegistroNaoEncontradoException, RegistroDuplicadoException. Verificar se
	 * a inserção de uma multa para um condutor existente apresenta sucesso.
	 * Utilizar os padrões Object Mother e Test Data Builder para instanciar
	 * Condutores e Multas.
	 */
	public void inserirCondutorDuplicado() {
		// 1. Utilizando o Object Mother / Test Data Builder, instanciar um
		// condutor.
		// 2. Inserir um condutor.
		// 3. Inserir a multa.
		// 4. Recuperar a multa, para ver se a mesma foi adequadamente associada
		// ao condutor e persistida.
	}

}
