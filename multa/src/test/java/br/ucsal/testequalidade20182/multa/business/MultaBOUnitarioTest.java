package br.ucsal.testequalidade20182.multa.business;

public class MultaBOUnitarioTest {

	/*
	 * Método a ser testado: public void inserir(Integer numeroCnh, Date
	 * dataHora, String local, TipoMultaEnum tipoMulta) throws
	 * RegistroNaoEncontradoException, RegistroDuplicadoException. Verificar se
	 * a inserção de uma multa para um condutor existente apresenta sucesso.
	 * Utilizar os padrões Object Mother e Test Data Builder para instanciar
	 * Condutores e Multas.
	 */
	public void inserirCondutorDuplicado() {
		// 1. Construir o mock para o CondutorDao.
		// 2. Construir o mock para o MultaDao.
		// 3. Instanciar um condutor utilizando o Object Mother / Test Data
		// 4. Builder.
		// 5. Definir o retorno específicos para cada mock.
		// 6. Inserir a multa.
		// 7. Verificar se a multa criada está adequadamente associada ao
		// condutor.
		// 8. Verificar a(s) chamada(s) ao(s) mock(s).
	}

}
