package br.ucsal.testequalidade20182.multa.business;

import java.util.Date;

import br.ucsal.testequalidade20182.multa.domain.Condutor;
import br.ucsal.testequalidade20182.multa.domain.Multa;
import br.ucsal.testequalidade20182.multa.domain.TipoMultaEnum;
import br.ucsal.testequalidade20182.multa.exception.RegistroDuplicadoException;
import br.ucsal.testequalidade20182.multa.exception.RegistroNaoEncontradoException;
import br.ucsal.testequalidade20182.multa.persistence.CondutorDao;
import br.ucsal.testequalidade20182.multa.persistence.MultaDao;

public class MultaBo {
	
	public MultaDao multaDao;
	public CondutorDao condutorDao;

	public MultaBo(MultaDao multaDao, CondutorDao condutorDao) {
		this.multaDao = multaDao;
		this.condutorDao = condutorDao;
	}

	public Multa inserir(Integer numeroCnh, Date dataHora, String local, TipoMultaEnum tipoMulta)
			throws RegistroNaoEncontradoException, RegistroDuplicadoException {
		Condutor condutor = condutorDao.encontrarByNumeroCnh(numeroCnh);
		Multa multa = new Multa(dataHora, local, tipoMulta, condutor);
		multaDao.inserir(multa);
		return multa;
	}

}
