package br.ucsal.testequalidade20182.multa.tui;

import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

import br.ucsal.testequalidade20182.multa.business.CondutorBo;
import br.ucsal.testequalidade20182.multa.business.MultaBo;
import br.ucsal.testequalidade20182.multa.domain.TipoMultaEnum;
import br.ucsal.testequalidade20182.multa.exception.RegistroDuplicadoException;
import br.ucsal.testequalidade20182.multa.exception.RegistroNaoEncontradoException;
import br.ucsal.testequalidade20182.multa.util.DateUtil;

public class CondutorTui {

	private Scanner sc = new Scanner(System.in);
	private CondutorBo condutorBo;
	private MultaBo multaBo;

	public CondutorTui(CondutorBo condutorBo, MultaBo multaBo) {
		this.condutorBo = condutorBo;
		this.multaBo = multaBo;
	}

	public void cadastrarCondutor() {
		Integer numeroCnh = obterNumeroCnh();
		String nome = obterNomeCondutor();
		cadastrarCondutor(numeroCnh, nome);
	}

	private void cadastrarCondutor(Integer numeroCnh, String nome) {
		try {
			condutorBo.inserir(numeroCnh, nome);
			System.out.println("Condutor cadastrado com sucesso.");
		} catch (RegistroDuplicadoException e) {
			System.out.println("Já existe um condutor cadastrado para esta CNH.");
		}
	}

	private String obterNomeCondutor() {
		System.out.println("Informe o nome do condutor:");
		String nome = sc.nextLine();
		return nome;
	}

	private Integer obterNumeroCnh() {
		System.out.println("Informe o número da CNH:");
		Integer numeroCnh = sc.nextInt();
		sc.nextLine();
		return numeroCnh;
	}

	public void cadastrarMulta() {
		Integer numeroCnh = obterNumeroCnh();
		Date dataHora = obterDataHora();
		String local = obterLocal();
		TipoMultaEnum tipoMulta = obterTipoMulta();

		cadastrarMulta(numeroCnh, dataHora, local, tipoMulta);
	}

	private void cadastrarMulta(Integer numeroCnh, Date dataHora, String local, TipoMultaEnum tipoMulta) {
		try {
			multaBo.inserir(numeroCnh, dataHora, local, tipoMulta);
			System.out.println("Multa cadastrada com sucesso.");
		} catch (RegistroNaoEncontradoException e) {
			System.out.println("Condutor não encontrado.");
		} catch (RegistroDuplicadoException e) {
			System.out.println("Já existe uma multa cadastrada para este condutor nesta data/hora.");
		}
	}

	private TipoMultaEnum obterTipoMulta() {
		System.out.println("Tipo da multa (" + TipoMultaEnum.listarTiposMulta() + "):");
		String tipoMultaString = sc.nextLine();
		TipoMultaEnum tipoMulta = TipoMultaEnum.valueOf(tipoMultaString);
		return tipoMulta;
	}

	private String obterLocal() {
		System.out.println("Local:");
		String local = sc.nextLine();
		return local;
	}

	private Date obterDataHora() {
		System.out.println("Data/hora:");
		Date dataHora = null;
		do {
			try {
				String dataHoraString = sc.nextLine();
				dataHora = DateUtil.parse(dataHoraString);
			} catch (ParseException e) {
				System.out.println("Data/hora inv�lida.");
			}
		} while (dataHora == null);
		return dataHora;
	}

}
