package br.ucsal.testequalidade20182.multa.persistence;

import java.util.ArrayList;

import br.ucsal.testequalidade20182.multa.domain.Multa;
import br.ucsal.testequalidade20182.multa.exception.RegistroNaoEncontradoException;

public class MultaDao {

	private ArrayList<Multa> multas = new ArrayList<>();

	public void inserir(Multa multa) {
		multas.add(multa);
	}

	public Multa encontrarByNumeroCnh(Integer numeroCnh) throws RegistroNaoEncontradoException {
		for (Multa multa : multas) {
			if (multa.getCondutor().getNumeroCnh().equals(numeroCnh)) {
				return multa;
			}
		}
		throw new RegistroNaoEncontradoException();
	}

}
