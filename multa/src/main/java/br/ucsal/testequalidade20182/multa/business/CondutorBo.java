package br.ucsal.testequalidade20182.multa.business;

import br.ucsal.testequalidade20182.multa.domain.Condutor;
import br.ucsal.testequalidade20182.multa.exception.RegistroDuplicadoException;
import br.ucsal.testequalidade20182.multa.persistence.CondutorDao;

public class CondutorBo {

	public CondutorDao condutorDao;

	public CondutorBo(CondutorDao condutorDao) {
		this.condutorDao = condutorDao;
	}

	public void inserir(Integer numeroCnh, String nome) throws RegistroDuplicadoException {
		Condutor condutor = new Condutor(numeroCnh, nome);
		condutorDao.inserir(condutor);
	}

}
