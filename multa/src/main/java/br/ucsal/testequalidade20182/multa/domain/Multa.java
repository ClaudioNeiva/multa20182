package br.ucsal.testequalidade20182.multa.domain;

import java.util.Date;

public class Multa {

	private Date dataHora;

	private String local;

	private TipoMultaEnum tipo;

	private Condutor condutor;

	public Multa(Date dataHora, String local, TipoMultaEnum tipo, Condutor condutor) {
		super();
		this.dataHora = dataHora;
		this.local = local;
		this.tipo = tipo;
		this.condutor = condutor;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public TipoMultaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoMultaEnum tipo) {
		this.tipo = tipo;
	}

	public Condutor getCondutor() {
		return condutor;
	}

	public void setCondutor(Condutor condutor) {
		this.condutor = condutor;
	}

	@Override
	public String toString() {
		return "Multa [dataHora=" + dataHora + ", local=" + local + ", tipo=" + tipo + ", condutor=" + condutor + "]";
	}

}
